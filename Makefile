up: docker-up-app
up-dev: docker-up-dev
down: docker-down
restart: docker-down docker-up-app
restart-dev: docker-down docker-up-dev
init-app:  docker-down-clear-app docker-pull-app docker-build-app docker-up-app initialization
init-dev: docker-down-clear-dev docker-pull-dev docker-build-app-dev docker-up-dev initialization

docker-up-app:
	ENV_FILE=app.env docker compose up -d

docker-up-dev:
	ENV_FILE=app_dev.env docker compose up -d

docker-down:
	 ENV_FILE=app.env docker compose down --remove-orphans

docker-down-clear-app:
	 ENV_FILE=app.env docker compose down -v --remove-orphans

docker-down-clear-dev:
	 ENV_FILE=app_dev.env docker compose down -v --remove-orphans

docker-pull-app:
	ENV_FILE=app.env docker compose pull

docker-pull-dev:
	 ENV_FILE=app_dev.env docker compose pull

docker-build-app:
	 ENV_FILE=app.env docker compose build --build-arg target=app

docker-build-app-dev:
	 ENV_FILE=app_dev.env docker compose build --build-arg target=app_dev

initialization:
	ENV_FILE=app_dev.env docker compose exec app sh /tmp/init.sh