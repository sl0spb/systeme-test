To set up the project, you need to:

1. Install Docker or have it already installed on your system;
2. Start the required services by running one of the following commands:
 ```
  make init-app
 ```
   to set up the project, or
  ```
  make init-dev
  ```
to set up the project with Xdebug, Psalm, and PHP-Fixer enabled.


I'm looking forward to your feedback, especially if the implementation seems unsuccessful.If some decisions seem questionable, I am ready to explain why I made them that way.