<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Country;
use App\Repository\ItemRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class IndexType extends AbstractType
{
	public function __construct(private readonly ItemRepository $itemRepository)
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options): void
	{
		$builder
			->add('item', ChoiceType::class, [
				'label'   => false,
				'choices' => $this->getItems(),
			])
			->add('taxNumber', TextType::class, [
				'label'       => false,
				'attr'        => [
					'placeholder' => 'Enter your tax number',
				],
				'constraints' => [
					new Assert\Length([
						'min'        => 3,
						'max'        => 20,
						'minMessage' => 'Tax number must be at least 3 characters long',
						'maxMessage' => 'Tax number cannot be longer than 20 characters',
					]),
					new Callback([
						'callback' => [$this, 'checkTaxNumber'],
					]),
				],
			]);
	}

	public function checkTaxNumber($value, ExecutionContextInterface $context): void
	{
		$code   = strtoupper(substr($value, 0, 2));
		$digits = substr($value, 2);

		if (!\in_array($code, Country::ALLOWED_CODES) || !ctype_digit($digits)) {
			$context->buildViolation('Tax number is incorrect')
					->atPath('taxNumber')
					->addViolation();
		}
	}

	private function getItems(): array
	{
		$result = [];

		foreach ($this->itemRepository->findAll() as $item) { // In a real project, I would avoid using findAll() here, of course
			$result[(string) $item->getTitle()] = $item->getId();
		}

		return $result;
	}
}
