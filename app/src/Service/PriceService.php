<?php

declare(strict_types=1);

namespace App\Service;

class PriceService
{
	public static function getFinalPrice(int $initialPrice, int $tax): string
	{
		$price = self::applyPercentTax($initialPrice, $tax);

		return number_format($price / 100, 2, '.', '');
	}

	// In a real project, I would use PhpMoney instead of this
	private static function applyPercentTax(int $initialPrice, int $tax): float
	{
		return $initialPrice * (1 + $tax / 100);
	}
}
