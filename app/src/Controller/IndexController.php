<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Item;
use App\Form\Type\IndexType;
use App\Repository\ItemRepository;
use App\Service\PriceService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
	public function __construct(private readonly ItemRepository $itemRepository)
	{
	}

	#[Route('/', name: 'index')]
	#[Template]
	public function __invoke(Request $request): array
	{
		$result = null;
		$form   = $this->createForm(IndexType::class)->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			/** @var Item $item */
			$item = $this->itemRepository->find($form->getData()['item']);

			$code = strtoupper(substr($form->getData()['taxNumber'], 0, 2));
			if ($tax = $item->getTaxByCodeOrNull($code)) {
				$result = sprintf(
					'%s costs %s€ in your country',
					(string) $item->getTitle(), PriceService::getFinalPrice((int) $item->getPrice(), $tax)
				);
			} else {
				$result = 'This item is not available in your country';
			}
		}

		return [
			'form'   => $form->createView(),
			'result' => $result,
		];
	}
}
