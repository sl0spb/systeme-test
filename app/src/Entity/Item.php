<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ItemRepository::class)]
class Item
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column]
	private ?int $id = null;

	#[ORM\Column(length: 255)]
	private ?string $title = null;

	#[ORM\Column]
	private ?int $price = null;

	#[ORM\Column(type: Types::TEXT, nullable: true)]
	private ?string $description = null;

	#[ORM\ManyToMany(targetEntity: Country::class)]
	#[Assert\Count(min: 1)]
	private Collection $countries;

	#[ORM\ManyToMany(targetEntity: Seller::class, mappedBy: 'items')]
	private Collection $sellers;

	public function __construct()
	{
		$this->countries = new ArrayCollection();
		$this->sellers   = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getTitle(): ?string
	{
		return $this->title;
	}

	public function setTitle(string $title): self
	{
		$this->title = $title;

		return $this;
	}

	public function getPrice(): ?int
	{
		return $this->price;
	}

	public function setPrice(int $price): self
	{
		$this->price = $price;

		return $this;
	}

	public function getDescription(): ?string
	{
		return $this->description;
	}

	public function setDescription(?string $description): self
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * @return Collection<int, Country>
	 */
	public function getCountries(): Collection
	{
		return $this->countries;
	}

	public function addCountry(Country $country): self
	{
		if (!$this->countries->contains($country)) {
			$this->countries->add($country);
		}

		return $this;
	}

	public function removeCountry(Country $country): self
	{
		$this->countries->removeElement($country);

		return $this;
	}

	/**
	 * @return Collection<int, Seller>
	 */
	public function getSellers(): Collection
	{
		return $this->sellers;
	}

	public function addSeller(Seller $seller): self
	{
		if (!$this->sellers->contains($seller)) {
			$this->sellers->add($seller);
			$seller->addItem($this);
		}

		return $this;
	}

	public function removeSeller(Seller $seller): self
	{
		if ($this->sellers->removeElement($seller)) {
			$seller->removeItem($this);
		}

		return $this;
	}

	public function getTaxByCodeOrNull(string $code): ?int
	{
		foreach ($this->getCountries() as $country) {
			if ($country->getCode() === $code) {
				return $country->getTax();
			}
		}

		return null;
	}
}
