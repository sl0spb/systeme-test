<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CountryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CountryRepository::class)]
class Country
{
	public const ALLOWED_CODES = ['DE', 'IT', 'GR'];
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column]
	private ?int $id = null;

	#[ORM\Column(length: 2, unique: true)]
	private ?string $code = null;

	#[ORM\Column(length: 64, nullable: false)]
	private ?string $title = null;

	#[ORM\Column(type: Types::SMALLINT)]
	private ?int $tax = null;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCode(): ?string
	{
		return $this->code;
	}

	public function setCode(string $code): self
	{
		$this->code = $code;

		return $this;
	}

	public function getTitle(): ?string
	{
		return $this->title;
	}

	public function setTitle(string $title): self
	{
		$this->title = $title;

		return $this;
	}

	public function getTax(): ?int
	{
		return $this->tax;
	}

	public function setTax(int $tax): self
	{
		$this->tax = $tax;

		return $this;
	}
}
