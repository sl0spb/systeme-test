<?php

declare(strict_types=1);

namespace App\Command;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
	name: 'await:database',
	description: 'Wait for database to come up',
)]
final class AwaitDatabaseCommand extends Command
{
	private string $lastError = '';

	public function __construct(
		private readonly Connection $connection,
	) {
		parent::__construct();
	}

	protected function configure(): void
	{
		$this
			->setName('await:database')
			->setDescription('Wait for database to come up');
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$timeStart = time();

		while (false === $this->isAvailable()) {
			if (time() - $timeStart > 10) {
				$output->writeln('Waiting for connection to db...');
			}

			// After 20 seconds, it displays a specific problem
			if (time() - $timeStart > 20) {
				$output->writeln($this->lastError);
			}

			sleep(1);
		}

		return 0;
	}

	private function isAvailable(): bool
	{
		try {
			$query = $this->connection->getDatabasePlatform()->getDummySelectSQL();
			$this->connection->executeQuery($query);

			return true;
		} catch (\Throwable $e) {
			$this->lastError = $e->getMessage();

			return false;
		}
	}
}
