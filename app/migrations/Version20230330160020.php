<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230330160020 extends AbstractMigration
{
	public function getDescription(): string
	{
		return '';
	}

	public function up(Schema $schema): void
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql('CREATE SEQUENCE country_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
		$this->addSql('CREATE SEQUENCE item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
		$this->addSql('CREATE SEQUENCE seller_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
		$this->addSql('CREATE TABLE country (id INT NOT NULL, code VARCHAR(2) NOT NULL, title VARCHAR(64) NOT NULL, tax SMALLINT NOT NULL, PRIMARY KEY(id))');
		$this->addSql('CREATE UNIQUE INDEX UNIQ_5373C96677153098 ON country (code)');
		$this->addSql('CREATE TABLE item (id INT NOT NULL, title VARCHAR(255) NOT NULL, price INT NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
		$this->addSql('CREATE TABLE item_country (item_id INT NOT NULL, country_id INT NOT NULL, PRIMARY KEY(item_id, country_id))');
		$this->addSql('CREATE INDEX IDX_582EEF32126F525E ON item_country (item_id)');
		$this->addSql('CREATE INDEX IDX_582EEF32F92F3E70 ON item_country (country_id)');
		$this->addSql('CREATE TABLE seller (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
		$this->addSql('CREATE TABLE seller_item (seller_id INT NOT NULL, item_id INT NOT NULL, PRIMARY KEY(seller_id, item_id))');
		$this->addSql('CREATE INDEX IDX_1DE2E0FE8DE820D9 ON seller_item (seller_id)');
		$this->addSql('CREATE INDEX IDX_1DE2E0FE126F525E ON seller_item (item_id)');
		$this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
		$this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
		$this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
		$this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
		$this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
		$this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
		$this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
		$this->addSql('ALTER TABLE item_country ADD CONSTRAINT FK_582EEF32126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
		$this->addSql('ALTER TABLE item_country ADD CONSTRAINT FK_582EEF32F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
		$this->addSql('ALTER TABLE seller_item ADD CONSTRAINT FK_1DE2E0FE8DE820D9 FOREIGN KEY (seller_id) REFERENCES seller (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
		$this->addSql('ALTER TABLE seller_item ADD CONSTRAINT FK_1DE2E0FE126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
	}

	public function down(Schema $schema): void
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql('CREATE SCHEMA public');
		$this->addSql('DROP SEQUENCE country_id_seq CASCADE');
		$this->addSql('DROP SEQUENCE item_id_seq CASCADE');
		$this->addSql('DROP SEQUENCE seller_id_seq CASCADE');
		$this->addSql('ALTER TABLE item_country DROP CONSTRAINT FK_582EEF32126F525E');
		$this->addSql('ALTER TABLE item_country DROP CONSTRAINT FK_582EEF32F92F3E70');
		$this->addSql('ALTER TABLE seller_item DROP CONSTRAINT FK_1DE2E0FE8DE820D9');
		$this->addSql('ALTER TABLE seller_item DROP CONSTRAINT FK_1DE2E0FE126F525E');
		$this->addSql('DROP TABLE country');
		$this->addSql('DROP TABLE item');
		$this->addSql('DROP TABLE item_country');
		$this->addSql('DROP TABLE seller');
		$this->addSql('DROP TABLE seller_item');
		$this->addSql('DROP TABLE messenger_messages');
	}
}
